/* This module has two purposes:
1) Retrieve a predefined role object given any user obj,
which adds additional application-specific details to classes of users
without duplicating data;

2) Each role can include a duration, indicating the registration period for each role.
Middleware can be added to routes which will adjust cookie maxAge to match the user's role duration
which remains since their intial login (or other startDate).

*/

var _ = require('lodash'),
		timediff = require('timediff'),
		minute = 60000,//These vals will be referenced by name and eval'd
    hour = 60*minute,
    day = 24*hour,
    week = 7*day,
    defaultRole = new UserRole('',{
			phrase:'a dummy user', //noun phrase for role name
			duration:'2*minute',// should eval to integer
			periodNoun:'two minutes', //noun phrase for duration
			periodAdj:'two-minute' //adjective phrase
			// App-defined roles may have additional properties
		}),
		roleObjs = null, //set when client calls userRole.setRoles()
		propIndex = { //default property names within a req.user object
			// Read-only properties of user:
			role:'role',            //user.role is one key of roles
			startDate:'startDate',  //user.startDate is a Date integer (or undefined)
			//endDate:'endDate',      //user.endDate is a Date integer (or undefined)
			// Write-only properties of user:
			status: 'status'        // adjustExpiration middleware will set this
		};


// Main export: factory returning shared role object
function userRole(userObj) {
	var roleName = (typeof userObj==='string')?
				userObj: //assume role name
				userObj[propIndex.role]; // assume req.user object --> req.user.role
	var roleObj = roleObjs && roleObjs[roleName];
	return roleObj || defaultRole;
}

//Ctor for pre-building each role obj
function UserRole(roleName,roleDefn) {
  // a role instance is stateless, not dependent on user.startDate
	this.name = roleName;
	//var roleDefn = (roles && roles[this.name]) || defaultRoleDef;
	_.extend(this,roleDefn);
	// add dynamically-computed fields:
	var duration = roleDefn.duration;
	// pattern-checking for some security...
	if (duration && !duration.match(/^(\d+\*)?\w+$/))
		return console.log("Invalid role duration:",roleDefn);
	this.duration = eval(duration);
}

// Public helper functions:
userRole.renameProperties = function(props) {
	// may be called by client to redefine or add more props of user record
  if (typeof props === 'object') {
    _.extend(propIndex,props);
  }
}

userRole.setRoles = function(roleDefs) {
  if (typeof roleDefs === 'object') {
		roleObjs = _.mapValues(roleDefs,
													(defn,name)=>(new UserRole(name,defn)))
	}
}

userRole.status = function(req) {
	// TODO: cache status in req and retrieve for subsequent calls
  // Status is a role qualified by a startDate and present time,
  // representing its current validity.
	// (Given a request, get additional details on remaining time, etc)
	var role = userRole(req.user);
	return role.status(req);
}

UserRole.prototype.status = function(req) {
	var	now = Date.now(), // Maybe use req date instead?
			duration = this.duration,//falsey--> unlimited
			expiration = duration?
                    (req.user[propIndex.startDate] || now)+duration :
						// BUG: (startDate )|| now) means that if startDate is missing,
						// user is always ok, with expiration calculated from now
                    null,
			remaining = duration?
                    Math.max(0,(expiration - now)) :
                    Infinity;
	return { //make a status object...
		role: this, // UserRole instance, including name and duration
    expiration: expiration, //null or positive #
		remaining: remaining, //0, positive finite, or Infinity
		ok: (remaining>0), //Boolean
    remainingApprox: function(){//generate a phrase for remaining time
      if (!expiration)
        return 'unlimited';
      var units = timediff(now,expiration,{
            units:"YMDHmS",//omit weeks, msec
            returnZeros:false
          }),
          useUnit = _.findKey(units),//first non-zero unit (e.g. 'days')
          val = units[useUnit];
      if (val==1) {
        useUnit = useUnit.slice(0,-1);//drop plural 's'
      }
      return "about "+val+" "+useUnit;
    }
	}
}

// Middleware for validating expiration and updating session cookie.
// Should be run after successful authentication but before delivering auth content.
userRole.adjustExpiration = function(whenExpired) {
  // whenExpired is optional middleware function to run when user time expires

  // Generate middleware...
  return function(req,res,next) {
  	//adjust session cookie duration by role:
    var user = req.user,
  	    status = user[propIndex.status] = userRole.status(req);
  	//req.session.cookie.maxAge = isFinite(status.remaining)? status.remaining: null;
    req.session.cookie.expires =
        status.expiration? new Date(status.expiration) : null;
  	return (status.ok)?
      next() : //proceed
      (whenExpired || userRole.whenExpired)(req,res,next); //run failure middleware
  }
}

// Default failure middleware for when user status has expired.
// Overriding this will change the default failure response for all uses of adjustExpiration.
userRole.whenExpired = function(req,res,next) {
  var err = new Error("User registration has expired");
  err.status = 403;
  next(err);
}

module.exports = userRole;
