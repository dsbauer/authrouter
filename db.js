var hash = require('pwd').hash;

var users = [
    { id: 1, username: 'jack', password: 'secret', displayName: 'Jack', emails: [ { value: 'jack@example.com' } ] }
  , { id: 2, username: 'jill', password: 'birthday', displayName: 'Jill', emails: [ { value: 'jill@example.com' } ] }
];

var items = [
  { id:1, title:"jack's thing", ownerId:1, ownerName:'jack'},
  { id:2, title:"jill's thing", ownerId:2, ownerName:'jill'}
];

var hashed = users.map(function(user) {
  var replacement = {
    id:user.id,
    username:user.username,
    displayName:user.displayName,
    emails:user.emails
  }
  hash(user.password, function(err,salt,hash) {
    replacement.salt = salt;
    replacement.hash = hash;
  })
  return replacement;
})

exports.findByUsername = function(username, cb) {
  var users = hashed;//use the hashed database instead of raw
  process.nextTick(function() {
    for (var i = 0, len = users.length; i < len; i++) {
      var user = users[i];
      if (user.username === username) {
        //console.log(user);
        return cb(null, user);
      }
    }
    return cb(null, null);
  });
}

// Interface is now obsolete, doesn't match mongodb version
exports.findUsersItems = function(ownername,unownedOK,cb) {
  process.nextTick(function() {
    var mine = items.filter(item=>(!item.ownerName && unownedOK) ||
                                  (item.ownerName===ownername));
    return cb(null,mine);
  })
}

exports.findByItemId = function(id, cb) {
  process.nextTick(function() {
    for (var i=0, len = items.length; i<len; i++) {
      var item = items[i];
      if (item.id == id) {
        return cb(null,item);
      }
    }
    return cb(null,null);
  })

}
