var mongoSanitize = require('express-mongo-sanitize');

var attackLogged = new Error('Malicious request blocked and logged');
attackLogged.status = 403;
var attackLogFail =new Error('Malicious request blocked; logging failed');
attackLogFail.status = 500;

// Middleware which detects any malicious mongodb operators (as per mongoSanitize.has)
//  in req.body for methods except GET.
// If found,
//  neutralizes operator by substituting _ for $ or .,
//  creates the flag req.threats.body, which can be checked later by quarantine()
function neutralize(req,res,next) {
	if (req.method==='GET')
		return next();// don't bother with GET routes, which should have no req.body
	//console.log('checking for threat...');
	if (req.threats)
		return next();// do only once
	req.threats = {};
	var threat = mongoSanitize.has(req.body);//detect any suspicious operators in req.body
	if (threat) {
		//console.log('found threat!');
		//note it:
		req.threats.body = threat;
		//render harmless:
		mongoSanitize.sanitize(req.body, {
			replaceWith: '_'
		});
	}
	// let later middleware intercept and/or quarantine...
	next();
}

function quarantine(db) {
	// Given a mongo database, generate middleware which
	//  checks for a threat flagged earlier by neutralize().
	// If found,
	//  stash as evidence in db.quarantine,
	//  then abort with error
	// Should be included after passport authentication if possible,
	//  so that username is available
	return function(req,res,next) {
		//console.log('checking quarantine...');
		if (req.threats && req.threats.body) {
			var time = Date.now(),
					url = req.originalUrl,
					username = (req.session.passport && req.session.passport.user)
										|| 'unknown';
			console.log('Quarantined request from',username,'to',url,'at',time);
			// already neutralized earlier, but log evidence to db.quarantine
			var record = {
				username: username,
				ip: req.ip,
				date: time,
				method: req.method,
				url: url,
				body: req.body
			}
			//console.log(record);
			db.createRecord('quarantine',record,function(err,result){
				if (err)
					return next(attackLogFail);
				return next(attackLogged);
			})
		} else {
			next();
		}
	}
}

exports.neutralize = neutralize;
exports.quarantine = quarantine;
