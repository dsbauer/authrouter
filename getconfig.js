var _ = require('lodash');

// Incrementally load configuration variables...

// Getter config(name) will return cached value, if any.
// If none, try to copy it from environment and cache for later.
var config = function(name) {
  if (config.hasOwnProperty(name))
    return config[name];
  if (!(name in process.env))
    throw ("Missing environment var: "+name)
  // cache as property for backward compatibility...
  return config[name] = process.env[name];
}

try {// use file config.js if present
  _.extend(config,require('../config')); //cache everything found
  console.log('Using local configuration file...')
} catch (e) {
  if (e.code !== 'MODULE_NOT_FOUND') throw e;
  console.log('No config file found; using environment to configure server...')
}

// Fill in any missing field from corresponding environment variable:
var mandatory = [
  'MONGO_URI',//uri for mongodb connection
  'MONGO_COLL',//name of main data collection in mongo
  'SESSION_SECRET'// for validating session cookies
];

mandatory.forEach(config);

module.exports = config;
