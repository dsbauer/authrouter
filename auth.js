// Augments a boilerplate express Router with certain methods of
//  passport and connect-ensure-login,
//  plus a payload sanitizer for mongodb
var express = require('express'),
    session = require('express-session'),
    errors = require('./errors');

// With mongodb:
var db = require('./mongodb.js'),
    MongoDBStore = require('connect-mongodb-session')(session),
    store = new MongoDBStore({uri:db.uri, collection:'sessions'});
// Dummy db with volatile session:
// var db = require('./db.js'),
//     store = new session.MemoryStore();//needs upgrade for production


var passport = require('passport'),
// Strategy = require('passport-http').BasicStrategy;
    Strategy = require('passport-local').Strategy;

var pwd = require('pwd'),
    hash = pwd.hash;

// Default password version getter (should return truthy string)
// Will probably be overridden within app.js:
pwd.getPwdVersion = (passwd)=>"0";

function reportPasswordAnomaly(password,username) {
  var len = password.length;
  console.log(`Anomalous password (length ${len}) for user:`,username);
}

function reportObsoletePassword(username,vTried,vStored) {
  console.log(`Obsolete password (${vStored}<${vTried}) for user:`,username);
}

function verifyHash(username, password, cb) {
  //console.log('validating pwd: ',password);
  db.findByUsername(username, function(err, user) {
    if (err || !user) //don't supply err (eg 404) to avoid alerting malefactors
      return cb(null, false);
    // begin Experimental:
    var vers = pwd.getPwdVersion(password);
    if (!vers) {
      reportPasswordAnomaly(password,username);
      return cb(null,false);
    }
    if (vers!==user.pwdVers) {
      reportObsoletePassword(username,vers,user.pwdVers);
      return cb(null,false);//wrong password version; will always fail, so skip rehashing
    }// end Experimental
    hash(password,user.salt,function(err,hash) {
      if (err)//unlikely: invalid password, user, or salt
        return cb(err);
      if (user.hash !== hash)
        return cb(null, false);//password fail
      return cb(null, user);//success
    });
  });
}

// TODO: consider converting to middleware to simplify use
function hashObj(password, cb) {
  // generate the core object for a user (salt, hash, pwdVers)
  // and pass it along to cb
  if (!password)
    return cb(null,null);
  var pwdVers = pwd.getPwdVersion(password);
  if (!pwdVers || typeof pwdVers !== 'string')
    return cb(errors.badPassword,null);
  hash(password,function(err,salt,hash) {
    if (err)
      return cb(err,null);
    return cb(null,{salt:salt, hash:hash, pwdVers:pwdVers});
  })
}

passport.use(new Strategy(verifyHash));

passport.serializeUser(function(user, done) {
  done(null, user.username);
});
passport.deserializeUser(function(username,done) {
  db.findByUsername(username,done);
})

var authsentry = require('connect-ensure-login'), //ensure login
		mongosentry = require('./sentry'), //sanitize for mongo
		config = require('./getconfig');//uses env vars if needed

var sessionConfig = config.session || {
  resave:false,
  saveUninitialized:false
};
if (!sessionConfig.secret)
  sessionConfig.secret = config.SESSION_SECRET;
sessionConfig.store = store;

// Make some middleware shared by all routers:
var sharedSession = session(sessionConfig),
		passportInit = passport.initialize(),
		passportSession = passport.session(),
		neutralize = mongosentry.neutralize,
		quarantine = mongosentry.quarantine(db);

// Middleware generator functions for specific routes:
function authenticate() {
  // defer to passport to authorize and persist session+cookie:
  return passport.authenticate.apply(passport,arguments);
}

// Optional middleware for specific routes:
function authorize(req,res,next) {
  // just check username+password, don't mess with session
  verifyHash(req.body.username,req.body.password,function(err,user){
    return (err || !user)?
      next(errors.noauth) :
      next();
  })
}

function ensureUsernameMatches (req,res,next) {
  // Should be run only after router.ensureLoggedIn
  // ensures that all relevant username strings agree
  var myname = req.session.passport.user;
  if (!myname)
    return next(errors.noauth);
  if (myname!==req.user.username) //db record
    return next(errors.inconsistent);

  var routename = req.params.username;  //route suffix
  if (!routename)
    return next(errors.nada);
  if (routename!==myname)
    return next(errors.notYours)
  // For PUT/POST routes, makes sure username on form matches username elsewhere
  if (req.method!=='GET' && req.body && myname!==req.body.username) //submitted form
    return next(errors.inconsistent);
  next();
}



module.exports = function() {//export a router factory

  var router = express.Router();

	// Don't use any middleware yet; let client module choose.
  // But export optional middleware & services...

	router.preamble = [ //standard suite for secure routes:
		neutralize, //neutralize any mongo threats in req.body:
		// let passport rebuild session:
		sharedSession,
		passportInit,
		passportSession,
		// now that client is identified, quarantine any pre-neutralized threats:
		quarantine
  ];

  router.db = db;
  router.hashObj = hashObj;
  router.ensureLoggedIn = authsentry.ensureLoggedIn;
  router.authenticate = authenticate;
  router.authorize = authorize;
  router.ensureUsernameMatches = ensureUsernameMatches;

  return router;
}
