var assert = require('assert'),
    MongoClient = require('mongodb').MongoClient,
    ObjectID = require('mongodb').ObjectID,
    config = require('./getconfig'),//insert env vars as needed
    uri = config.MONGO_URI,
    itemCollectionName = config.MONGO_COLL;

var database = null,
    errors = require('./errors');

var client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect(function(err, client) {
  var db = client.db();
  assert.equal(null, err);
  var host = uri.match(/([^@/]+)(:\d+)?\/[^/]+$/);
  console.log("Connected to MongoDB server: ",host && host[1]);
	console.log("Using database",db.databaseName,"on",client.s.options.srvHost);
  database = db;
});

function getCollection(collName) {
  return database && database.collection(collName);
}
// Public for now so that admin utilities (e.g. password reset) can use esoteric MongoDB operations.
// But probably TEMP, since this bypasses any sanitation in this module.
exports.getCollection = getCollection;

function badKey(key) {//reject any object except for valid ObjectIDs
  return (typeof key==='object' && !ObjectID.isValid(key));
}

function findUniqueRecord(collName,keyName,keyVal,cb) {
  var collection = getCollection(collName);
  if (!collection) return cb(errors.noDB,null);
  var query = {};
  // TODO: sanitize better
  if (badKey(keyVal))
    return cb(errors.badKey,null);
  query[keyName]=keyVal;
  collection.find(query).toArray(function(err, docs) {
    //assert.equal(docs.length,1);
    if (!docs.length)
      return cb(errors.nada,null); //none found
    if (docs.length>1) //multiple found
      return cb(errors.multiple,null);
    return cb(null, docs[0]); //found single record
  })
}

function deleteRecord(collName,id,cb) {
  var collection = getCollection(collName);
  if (!collection) return cb(errors.noDB,null);
  collection.deleteOne({_id:ObjectID(id)},function(err,result) {
    if (err) {
      return cb(errors.update,null);
    }
    cb(null,result);
  })
}

function modifyOrReplaceRecord(collName,id,changes,cb,methodName) {
  // methodName is "updateOne" or "replaceOne"
  if (!ObjectID.isValid(id)) {
    return cb(errors.badID,null);
  }
  var collection = getCollection(collName);
  if (!collection) return cb(errors.noDB,null);
  collection[methodName]({_id:ObjectID(id)},changes,function(err,result) {
    if (err) {
      return cb(errors.update,null);
    }
    cb(null,result);
  })
}

// (currently) PUBLIC METHODS:
function modifyRecord(collName,id,changes,cb) {
  var replacement = {$set: changes};
  return modifyOrReplaceRecord(collName,id,replacement,cb,'updateOne');
}

function replaceRecord(collName,id,replacement,cb) {
  return modifyOrReplaceRecord(collName,id,replacement,cb,'replaceOne');
}

function createRecord(collName,record,cb) {
  var collection = getCollection(collName);
  if (!collection) return cb(errors.noDB,null);
  // TODO: sanitize record?
  collection.insertOne(record,function(err,result){
    if (err) {
      return cb(errors.insert,null);
    }
    var newid = result.insertedId;
    //console.log('Inserted item: ',newid);
    var reply = result.ops[0];
    //console.log('Replying with obj:',reply);
    return cb(null,reply);
  })
}
exports.createRecord = createRecord;
exports.modifyRecord = modifyRecord;
exports.replaceRecord = replaceRecord;


exports.findByUsername = function(username, cb) {
  return findUniqueRecord('users','username',username,cb);
}
exports.findByUserId = function(id, cb) {
  if (!ObjectID.isValid(id)) {
    return cb(errors.badID,null);
  }
  return findUniqueRecord('users','_id',ObjectID(id),cb);
}

var ownershipPolicies = {
  mine: function(username) {
    return {ownerName:username}
  },
  mineOrNobodys: function(username) {
    return {"$or":[{ownerName:username},{ownerName:{"$exists":false}}]}
  },
  anyones: function() {
    return null;
  }
}

exports.findUsersItems = function(username, policyName, cb, otherCollection) {
  if (badKey(username))
    return cb(errors.badKey,null);
  var collection = getCollection(otherCollection || itemCollectionName);
  if (!collection) return cb(null,null);
  var policyFn = ownershipPolicies[policyName] || ownershipPolicies.mine,
      query = policyFn(username);
  collection.find(query).toArray(cb);
}

exports.findByItemId = function(id, cb) {
  if (!ObjectID.isValid(id)) {
    return cb(errors.badID,null);
  }
  return findUniqueRecord(itemCollectionName,'_id',ObjectID(id),cb);
}

exports.updateItem = function(id, item, cb) {
  if (!ObjectID.isValid(id))
    return cb(errors.badID,null);
  return replaceRecord(itemCollectionName,id,item,cb);
}

exports.deleteItem = function(id, cb) {
  if (!ObjectID.isValid(id)) {
    return cb(errors.badID,null);
  }
  return deleteRecord(itemCollectionName,id,cb);
}

exports.createItem = function(item, cb) {
  return createRecord(itemCollectionName,item,cb);
}

exports.uri = uri;

exports.disconnect = function () {
  if (database)
    database.close();
}
