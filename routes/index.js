var router = require('../auth.js')(),
		userRole = require('../userrole.js');

router.use(router.preamble);

module.exports = function(opts) {
	//opts may include various middleware

	router.get('/',
		router.ensureLoggedIn('/noauth'),

		opts && opts.preContent || [], //optional middleware

		function(req, res) {
			//non-essential extra cookie for detecting expiration of session cookie:
			res.cookie('loginStatus',true);
			// deliver the goods:
			res.render('index',{
					layout:'auth-layout',
					username: req.user.username,
					version: req.app.locals.repoVersion,
					environment: req.app.locals.environment
			});
		});

	router.post('/authorize',	//AJAX only
		//validates user login without setting cookie, just replies true.
		// Should be used only to signal client that a password will work
		// for other auth routes
		// TODO: allow only if logged in, add username parameter
		router.authorize,
		function(req,res) {
			res.json(true);
		}
	)

	router.post('/login',
		router.authenticate('local',{failureRedirect: '/login?nope'}),
		userRole.adjustExpiration(), //adjust session cookie duration; may run userRole.whenExpired
		function(req, res) {
			return res.redirect('/');
		}
	)

	router.get('/login', function(req,res) {
		//req.query will have at most one key, which sets req.message
		if ('nope' in req.query)
			req.message='Login failed';
		var attrs = {
			subtitle:'Login',
			msg: req.message || 'Please sign in'
		}
		res.render('login',attrs);
	})

	router.get('/logout', function(req,res) {
		req.logout();
		res.clearCookie('loginStatus');
		res.redirect('/');
	})

	return router;
}
