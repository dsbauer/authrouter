// Generic router for authorized CRUD of user-owned items
var router = require('../auth.js')(),
		errors = require('../errors.js');

//TODO: this is weird... rewrite as middleware?
function ensureOwner(req,res,next,isShareable,proceed) {
  var id = req.params.id;
  router.db.findByItemId(id,function(err,item) {
    if (err)
      next(err);
    else if (!item) //no err, but can't find item
      next(errors.nada);
    else if (item.ownerName && item.ownerName === req.session.passport.user) //mine
      proceed(item,id);
    else if (isShareable(item)) //not mine but viewable
      proceed(item,id);
    else //someone else owns it and isnt sharing
      next(errors.notYours);
  })
}

function okIfUnowned(item) {
  return (!item.ownerName);
}
function okNever(item) { return false; }
function okAlways(item) { return true; }

router.use(router.preamble);

router.get('/', //get everything not owned by another
  router.ensureLoggedIn('/noauth'),
  function(req,res,next) {
    router.db.findUsersItems(req.session.passport.user,'mineOrNobodys',function(err,results){
      if (err) return next(err);
      //console.log(results.length);
      res.json(results);
    })
  }
)

router.get('/:id', //get a particular item, but only if no one else owns it
  router.ensureLoggedIn('/noauth'),
  function(req,res,next) {
    ensureOwner(req,res,next,okAlways,function(item,id) {
      res.json(item);
      // Testing alternative: render a page with an update button for easy update testing
      // res.render('item',{data:JSON.stringify(item), id:id});
    })
  }
)

router.delete('/:id',
  router.ensureLoggedIn('/noauth'),
  function(req,res,next) {
    ensureOwner(req,res,next,okNever,function(currItem,id) {
      router.db.deleteItem(id,function(err,result) {
        if (err)
          return next(err);
        //console.log('deleting record:',result && result.result);
				res.json({});
      })
    })
  }
)

router.put('/:id',
  router.ensureLoggedIn('/noauth'),
  function(req,res,next) {
    ensureOwner(req,res,next,okNever,function(currItem,id) {
      //double-check req.body for correct id (and owner?)
      if (req.body._id !== id)//wrong id for this route
        return next(errors.misdirectedID);
      delete req.body._id;//strip ID from record before db replacement
      //TODO: cleanse req.body...
      router.db.updateItem(id,req.body,function(err,result) {
        if (err)
          return next(err);
        //console.log('updating record:',result && result.result);
				res.json({});
				//BUG: mixes in mongo fields w. BB attrs:
        //res.json(result && result.result);
      })
    })
  }
)

router.post('/', //submit a new item
  router.ensureLoggedIn('/noauth'),
  function(req,res,next) {
    var item = req.body;
    //TODO: cleanse item...
    item.ownerName = req.session.passport.user;//set owner
    router.db.createItem(item,function(err,item) {
      if (err)
        return next(err);
      res.status(201).send(item);
    })
  }
)

module.exports = router;
