var express = require('express');
var router = express.Router();

// Any non-authenticated request will be redirected here, and the appropriate reply sent
// This is only needed b/c module ensureLoggedIn can only respond to failure with a redirect
function noAuth(req,res) {
	// If AJAX request:
	if (req.xhr)
		return res.status(401).send('Unauthorized');//send failure reply
	if (req.cookies.loginStatus)
		return res.redirect('/login?expired');
	res.redirect('/login');
}

// Add status messages for /login?status
router.use('/login',function(req,res,next) {
	if ('expired' in req.query)
    req.message='Your session has expired; please sign in again.';
  next();//call main /login handler
})

router.all('/noauth', noAuth);

module.exports = router;
