var _ = require('lodash'),
    router = require('../auth.js')(),
    errors = require('../errors'),
    userRole = require('../userrole.js'),
    validator = require('../validation.js'),
    isSubset = require('is-subset');

router.use(router.preamble);

module.exports = function(opts) {
//Valid opts:
// notifyOnPasswordChange: sync function(userObject)


//TODO: These may be too specific for authrouter; move to app-specific route?

// Change user record via AJAX request...
router.put('/:username',
  //req.body must include username and password for authorization,
  // plus other properties to be changed in db user object
  // (e.g. password1&2, email)

  router.ensureLoggedIn('/noauth'),
  router.ensureUsernameMatches,
  router.authorize,  //checks username+password
  validator, //initialize validator
  validator.checkNewPasswordIfIncluded, //gather any password problems
  // add validation for other fields here...

  validator.bounceAjaxIfInvalid,

  function(req,res,next) {// everything checks out; update user record...
    var newPwd = req.body.password1;//maybe undefined
    router.hashObj(newPwd,function(err,changes) {
      if (err) return next(err);
      // if newPwd, changes already contains {salt,hash}
      //changes = _.defaults(changes,{ //mix in desired properties from req.body...
        //email: req.body.email
      //})

      if (!changes || isSubset(req.user,changes))
        return res.json(false);//nothing to update

      var id = req.user._id;
      router.db.modifyRecord('users',id,changes,function(err,result){
        if (err) return next(err);
        var ok = result.result && result.result.ok;
        if (ok && opts && opts.notifyOnPasswordChange && newPwd) {
          opts.notifyOnPasswordChange(req.user);
        }
        return res.json(ok);
      })
    })
  }
)

router.get('/:username',
  router.ensureLoggedIn('/noauth'),
  router.ensureUsernameMatches,

  // TODO: sanitize req.params.username?
  function(req,res,next) {
    var reqname = req.params.username;
    router.db.findByUsername(reqname,function(err,user) {
      if (err)
        return next(err);
      if (!user)//shouldn't happen, but...
        return next(errors.nada);
      //cleanse response...
      var visibleUser = _.pick(user,['username','role','startDate','emails','email']);
      // convert db array to single value:
      visibleUser.email = visibleUser.email || visibleUser.emails[0];
      //delete visibleUser.emails; //emails array is DEPRECATED, but leave for now

      var status = userRole.status(req);
      // mix in some properties from status object:
      _.defaults(visibleUser,{
        timeRemaining:status.remainingApprox(),
        rolePhrase:status.role.phrase
      });
      res.json(visibleUser);
    })
  }
)

return router;
}
