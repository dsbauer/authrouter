function makeErr(str,status) {
  var err = new Error(str);
  err.status = status;
  return err;
}

exports.badID = makeErr('Malformed ID',404);//retrieving invalid db id
exports.badKey = makeErr('Malformed Key',400);//setting invalid db string key
exports.noDB = makeErr('Database Unready',500);
exports.nada = makeErr('No such key',404);//iz not thing
exports.multiple = makeErr('Multiple results for key',500);
exports.update = makeErr('Update failure',500);
exports.insert = makeErr('Insert failure',500);
exports.notYours = makeErr('Not yours',401);//u no can haz
exports.noauth = makeErr('Unauthorized',401);
// requested new password is unsuitable or mismatched:
exports.badPassword = makeErr('Unsuitable password',400);

// Model with id has somehow gone to wrong route:
exports.misdirectedID = makeErr('Misdirected ID',400);

// Request has inconsistent username/wrong route:
exports.inconsistent = makeErr('Inconsistent username',401);
