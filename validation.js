var _ = require('lodash'),
		expressValidator = require('express-validator');

//var OKnames = /^[-a-z0-9_.]*$/i;
function hasNoBadChars(str,rexp,details) {
	if (!str) return true;
	var badList = str.match(rexp);
	if (details && typeof details==='object') {
		details.toString = ()=>(badList && badList.join(''));
	}
	return !badList;
}

/*
var passwordSchema = {
	isLength:{
		options:{min:6},
		errorMessage: 'Password too short'
	},
	hasNoBadChars:{
		errorMessage:'Aaagh!  Bad chars!'
	},
	notEmpty: true,
	errorMessage: 'Invalid Password'
};
var schema = {
	'password1': passwordSchema,
	'password2': passwordSchema
}*/

module.exports = expressValidator({
	customValidators:{
		hasNoBadChars: hasNoBadChars,
		//passes: (str,arg)=>arg,
		failsIf: (str,arg)=>!arg
	}
});

//module.exports.schema = schema;

// Middleware:

var BadPwdChars = /[\s]/g;
module.exports.checkNewPassword = function(req,res,next) {
	var details={};
	//req.check(validator.schema);
	req.check('password1').isLength({min:8})
			.withMessage("Password length must be \u2265 8");
	//req.check('password1').hasNoBadChars(BadPwdChars,details) //TEMP...
	//		.withMessage("Contains bad chars: "+details);
	req.check('password2').equals(req.body.password1)
			.withMessage("These passwords don't match");
	next();
}

module.exports.checkNewPasswordIfIncluded = function(req,res,next) {
	if (('password1' in req.body) || ('password2' in req.body))
		return module.exports.checkNewPassword(req,res,next);
	next();
}

var BadNameChars = /[^\w_.]/g;
module.exports.checkUsername = function(req,res,next) {
	var details = {};
	req.check('username').isLength({min:5})
			.withMessage("Username length must be \u2265 5");
	req.check('username').isLength({max:20})
			.withMessage("Username length must be \u2264 20");
	req.check('username').hasNoBadChars(BadNameChars,details)
			.withMessage("Username contains invalid characters ("+details+")");
	next();
}

module.exports.checkEmail = function(req,res,next) {
	req.check('email').isEmail()
			.withMessage("Invalid email format");
	next();
}

// Middleware to re-render form with hints if any fields are invalid:
module.exports.bounceFormIfInvalid = function(template,opts) {
	// Generate route-specific middleware...

	// This presumes validator checks have already run on all fields...
	return function(req,res,next) { //validate form, maybe send back
		req.getValidationResult().then(function(problems) {
			if (problems.isEmpty()) {
				// Checked fields seem OK, proceed...
				return next();
			}
			// Else re-render form for corrections...
			// Build template options object:
			var details = _.extend({},
				opts, //static properties shared by all requests
				res.locals.bounceOpts, // properties specific to this req/res
				req.body // form input values
			);
			// Include object enumerating problems found:
			details.problems = _.mapValues(problems.mapped(),'msg');
			// TODO: log sketchy attempts?
			res.render(template,details);
		})
	}
}

// AJAX version: needs no template, shared by all routes
module.exports.bounceAjaxIfInvalid = function(req,res,next) {
	req.getValidationResult().then(function(problems) {
		if (problems.isEmpty()) {
			return next(); //proceed
		}
		// If problems, reply with array of error messages only (no values)
		var errs = _.map(problems.array(),'msg');
		res.status(400).json(errs);
		// TODO: generate pseudo-error to log message?
  })
}
